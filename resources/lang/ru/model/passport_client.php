<?php

return [
    'one' => 'Клиент',
    'many' => 'Клиенты',
    'singular' => 'клиент',
    'plural' => 'клиенты',
    'labels' => [
        'user' => 'Пользователь',
        'name' => 'Имя',
        'provider' => 'Провайдер',
        'redirect' => 'Redirect URL',
    ],
];
