<?php

namespace App\Library\Backpack;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanel as BackpackCrudPanel;
use Illuminate\Support\Str;
use ReflectionClass;

class CrudPanel extends BackpackCrudPanel
{

    public function __construct()
    {
        parent::__construct();

        $this->setLabeller(function (string $name) {
            $labels = trans('model/' . Str::snake((new ReflectionClass($this->model))->getShortName()) . '.labels');
            if (!is_array($labels) || !array_key_exists($name, $labels))
                return trim(mb_ucfirst(str_replace('_', ' ', preg_replace('/(_id|_at|\[\])$/i', '', $name))));
            return $labels[$name];
        });
    }

    /**
     * Set the label of a field, if it's missing, by capitalizing the name and replacing
     * underscores with spaces.
     *
     * @param array $field Field definition array.
     * @return array        Field definition array that contains label too.
     */
    protected function makeSureFieldHasLabel($field)
    {
        if (!isset($field['label'])) {
            $name = is_array($field['name']) ? $field['name'][0] : $field['name'];
            $field['label'] = mb_ucfirst($this->makeLabel($name));
        }

        return $field;
    }

}
