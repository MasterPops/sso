<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Laravel\Passport\Client;

class PassportClient extends Client
{
    use CrudTrait;

}
